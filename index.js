var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
const qs = require("qs");

var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
const axios = require("axios");
var app = express();

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.use("/users", usersRouter);
app.post("/sentMessage", (req, res) => {
  let { message } = req.body;
  let responseStatus;
  try {
    const data = axios.post(
      "https://notify-api.line.me/api/notify",
      qs.stringify({
        message: message,
      }),
      {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization: "Bearer lSTMmIMaNU8FEcK48YpOd6dGZoxxzSXtJP3Y0HR5a38",
        },
      }
    );
    console.log("message ",message);
    res.send("success!");
  } catch (error) {
    res.send("respond with a resource");
  }
});
app.listen(3000, () => {
  console.log("Running on port 3000.");
});
module.exports = app;
